#!/usr/bin/env python
"""lab4 assignment"""
import sys

# importing the necessary modules and libraries
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.impute import SimpleImputer
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
# pylint: disable-msg=E0611

from common import describe_data as describe_data
from common import test_env as test_env


def read_data(file):
    """Return pandas dataFrame read from csv file"""
    try:
        return pd.read_csv(file, sep=';', decimal=',', thousands=' ',
                           encoding='latin-1')
    except FileNotFoundError:
        sys.exit('ERROR: ' + file + ' not found')


def plot_clusters(X, y, figure, file=''):
    """Plotting clusters"""
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
              'tab:brown', 'tab:pink', 'tab:olive']
    markers = ['o', 'X', 's', 'D']
    color_idx = 0
    marker_idx = 0

    plt.figure(figure)

    for cluster in range(0, len(set(y))):
        plt.scatter(X[y == cluster, 0], X[y == cluster, 1],
                    s=5, c=colors[color_idx], marker=markers[marker_idx])
        color_idx = 0 if color_idx == (len(colors) - 1) else color_idx + 1
        marker_idx = 0 if marker_idx == (len(markers) - 1) else marker_idx + 1

    plt.title(figure)
    # Removing axes numbers
    plt.xticks([])
    plt.yticks([])

    if file:
        plt.savefig(file, papertype='a4')

    plt.show()


if __name__ == '__main__':
    modules = ['numpy', 'matplotlib', 'pandas', 'sklearn']
    test_env.versions(modules)

    # reading data
    df = pd.read_csv('data/cc_general.csv')

    X = df.iloc[:, 2:].values.astype(float)

    # extracting the unique identifier
    df.drop(columns='CUST_ID', inplace=True)

    # replacing missing values with mean
    imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    imp = imp.fit(X[:, 2:])
    X[:, 2:] = imp.transform(X[:, 2:])

    # using the elbow method plot
    wcss = []
    MAX_CLUSTERS = 13

    for i in range(1, MAX_CLUSTERS):
        k_means = KMeans(n_clusters=i, init='k-means++', random_state=0)
        k_means.fit(X)
        wcss.append(k_means.inertia_)

    plt.plot(range(1, MAX_CLUSTERS), wcss)
    plt.title('The Elbow Method')
    plt.xlabel('Number of clusters')
    plt.ylabel('WCSS')
    plt.show()
    plt.savefig('results/plot_1_WCSS_Elbow_Method.png')

    # visualising with t-sne
    X_tsne = TSNE(n_components=2, random_state=0).fit_transform(X)

    # creating a fake array with one cluster
    plot_clusters(X_tsne, np.full(X_tsne.shape[0], 0),
                  't-SNE visualisation without clusters')
    plt.savefig('results/plot_2_TSNE_Without_Clusters.png')

    y_kmeans = k_means.fit_predict(X)
    plot_clusters(X_tsne, y_kmeans, 'k means clusters with TSNE')
    plt.savefig('results/plot_3_TSNE_with_K_means_clusters.png')

    # saving data description overview to a file in results
    describe_data.print_overview(
        df, file='results/cc_overview.txt')

    print('Done')
